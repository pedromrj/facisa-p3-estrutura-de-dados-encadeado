package br.unifacisa.p3.utils;



import java.util.EmptyStackException;

import br.unifacisa.p3.excecao.EmptyDequeException;
import br.unifacisa.p3.excecao.EmptyQueueException;
import br.unifacisa.p3.excecao.InvalidObjectException;

/**
 * Classe para validar restricoes
 * @author Pedro
 *
 */
public class Validar {
	
	/**
	 * Metodo para validar se objeto e null
	 * @param obj Object
	 */
	public static void checkNull(Object obj) {
		if (obj == null) {
			throw new InvalidObjectException("Objeto null");
		}
	}
	
	/**
	 * Metodo para verificar se o indice corresponde ao tamanho
	 * @param indice int
	 * @param tamanho int
	 */
	public static void checkArray(int indice, int tamanho) {
		if (indice >= tamanho || indice < 0) {
			throw new ArrayIndexOutOfBoundsException();
		}
	}
	
	/**
	 * Metodo para checar o tamanho da Fila
	 * @param tamanho int
	 */
	public static void checkFila(int tamanho) {
		if (tamanho == 0) {
			throw new EmptyQueueException();
		}
	}
	
	/**
	 * Metodo para checar o tamanho da Pilha
	 * @param tamanho int
	 */
	public static void checkPilha(int tamanho) {
		if (tamanho == 0) {
			throw new EmptyStackException();
		}
	}
	
	/**
	 * Metodo para checar o tamanho da Deque
	 * @param tamanho int
	 */
	public static void checkDeque(int tamanho) {
		if (tamanho == 0) {
			throw new EmptyDequeException();
		}
 	}
	
}
