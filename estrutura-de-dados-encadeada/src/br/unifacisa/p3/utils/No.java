package br.unifacisa.p3.utils;

/**
 * Classe usada para referencia o proximo objeto dentro da Colecao
 * @author Pedro
 */
public class No<T> {

	private No<T> proximo;
	
	private No<T> anterior;

	private T obj;

	/**
	 * Metodo construtor para instanciar um No
	 * @param obj T
	 */
	public No(T obj) {
		super();
		this.obj = obj;
	}

	/**
	 * Metodo usado a retornar o proximo No
	 * @return No
	 */
	public No<T> getProximo() {
		return proximo;
	}

	/**
	 * Metodo usado para ajustar o proximo No
	 * @param proximo No
	 */
	public void setProximo(No<T> proximo) {
		this.proximo = proximo;
	}

	/**
	 * Metodo usado para retornar o objeto
	 * @return T
	 */
	public T getObj() {
		return obj;
	}

	/**
	 * Metodo usado para ajustar o objeto
	 * @param obj T
	 */
	public void setObj(T obj) {
		this.obj = obj;
	}
	

	/**
	 * Metodo usado para retornar o objeto
	 * @return anterior No
	 */
	public No<T> getAnterior() {
		return anterior;
	}
	
	/**
	 * Metodo usado para ajustar o objeto
	 * @param anterior No
	 */
	public void setAnterior(No<T> anterior) {
		this.anterior = anterior;
	}
	
	/**
	 * Retorna o objeto em formato de texto
	 */
	@Override
	public String toString() {
		return String.valueOf(obj);
	}
	
}
