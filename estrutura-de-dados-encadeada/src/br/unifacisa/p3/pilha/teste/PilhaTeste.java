package br.unifacisa.p3.pilha.teste;


import java.util.EmptyStackException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.unifacisa.p3.excecao.InvalidObjectException;
import br.unifacisa.p3.pilha.Pilha;

/**
 * Testes para uso da Pilha;
 * @author Pedro
 *
 */
class PilhaTeste {
	
	/**
	 * Objeto Pilha usado para teste;
	 */
	private Pilha<String> pilha;
	
	/**
	 * Metodo usado para resetar a pilha a cada teste
	 * @throws Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		this.pilha = new Pilha<String>();
	}
	
	/**
	 * Teste usado inserir objeto na pilha
	 */
	@Test
	void pushTeste1() {
		
		for (int i = 0; i < 5; i++) {
			pilha.push("Teste " + i);
		}
		
		Assertions.assertEquals("[Teste 4, Teste 3, Teste 2, Teste 1, Teste 0]", pilha.toString());
		
		for (int i = 0; i < 5; i++) {
			pilha.push("Teste " + i);
		}
		
		Assertions.assertEquals("[Teste 4, Teste 3, Teste 2, Teste 1, Teste 0, Teste 4, Teste 3, Teste 2, Teste 1, Teste 0]", pilha.toString());
		
	}
	
	/**
	 * Teste usado para inserir objeto Null no objeto
	 */
	@Test
	void pushTeste2() {
		
		Assertions.assertThrows(InvalidObjectException.class, () -> {
			pilha.push(null);
		});
		
	}
	
	/**
	 * Testando metodo push, verificando se passando null a pilha continua vazia
	 */
	@Test
	void pushTeste3() {
		Assertions.assertThrows(InvalidObjectException.class, () -> {
			pilha.push(null);
		});
		
		Assertions.assertEquals("[]", pilha.toString());
	}
	
	
	/**
	 * Teste usado para saber o tamanho, metodo da classe abstrata Colecao
	 */
	@Test
	void sizeTeste1() {

		for (int i = 0; i < 3; i++) {
			pilha.push("Teste " + i);
		}

		Assertions.assertEquals(3, pilha.size());

	}
	
	/**
	 * Teste usado para saber o tamanho da pilha vazia, metodo da classe abstrata Colecao
	 */
	@Test
	void sizeTeste2() {

		Assertions.assertEquals(0, pilha.size());

	}
	
	/**
	 * Testando metodo size, ao dar um pop na pilha
	 */
	@Test
	void sizeTeste3() {
		for (int i = 0; i < 3; i++) {
			pilha.push("Teste " + i);
		}
		
		Assertions.assertEquals(3, pilha.size());
		
		pilha.pop();
		Assertions.assertEquals(2, pilha.size());
	}
	
	/**
	 * Testando metodo size, verificar um pilha com elementos e adicionar mais elementos
	 */
	@Test
	void sizeTeste4() {

		for (int i = 0; i < 3; i++) {
			pilha.push("Teste " + i);
		}

		Assertions.assertEquals(3, pilha.size());
		pilha.push("Teste 1");
		Assertions.assertEquals(4, pilha.size());

	}
	
	/**
	 * Teste usado para saber se a pilha esta vazia, metodo da classe abstrata Colecao
	 */
	@Test
	void isEmptyTeste1() {
		
		Assertions.assertTrue(pilha.isEmpty());
		
	}
	
	/**
	 * Teste usado para saber se a pilha esta cheia, metodo da classe abstrata Colecao
	 */
	@Test
	void isEmptyTeste2() {

		for (int i = 0; i < 3; i++) {
			pilha.push("Teste " + i);
		}

		Assertions.assertFalse(pilha.isEmpty());

	}
	
	/**
	 * Testando metodo isEmpty ao dar um pop na pilha
	 */
	@Test
	void isEmptyTeste3() {

		for (int i = 0; i < 3; i++) {
			pilha.push("Teste " + i);
		}

		Assertions.assertFalse(pilha.isEmpty());
		pilha.pop();
		Assertions.assertFalse(pilha.isEmpty());

	}
	
	/**
	 * Testando a funcionalidade do metodo pop para remove
	 */
	@Test
	void popTeste1() {
		
		for (int i = 0; i < 3; i++) {
			pilha.push("Teste " + i);
		}
		
		Assertions.assertEquals("Teste 2", pilha.pop());
		
		Assertions.assertEquals("[Teste 1, Teste 0]", pilha.toString());
		
	}
	
	/**
	 * Testando a funcionalidade do metodo pop para um pilha vazia
	 */
	@Test
	void popTeste2() {
		
		Assertions.assertThrows(EmptyStackException.class, () -> {
			pilha.pop();
		});
	}
	
	/**
	 * Testando metodo pop, verificando o objeto retornado
	 */
	@Test
	void popTeste3() {
		for (int i = 0; i < 3; i++) {
			pilha.push("Teste " + i);
		}
		
		Assertions.assertEquals("Teste 2", pilha.pop());
	}
	
	/**
	 * Testando metodo pop, com um elemento na pilha
	 */
	@Test
	void popTeste4() {
		pilha.push("Teste 1");
		Assertions.assertEquals("Teste 1", pilha.pop());
	}
	
	/**
	 * Testando o metodo top, em uma pilha vazia
	 */
	@Test
	void topTeste1() {
		Assertions.assertThrows(EmptyStackException.class, () -> {
			pilha.top();
		});
	}
	
	/**
	 * Testando a funcionalidade do metodo top, na busca de valor do topo da pilha
	 */
	@Test
	void topTeste2() {
		
		for (int i = 0; i < 3; i++) {
			pilha.push("Teste " + i);
		}
		
		Assertions.assertEquals("Teste 2", pilha.top());
		
	}
	
	/**
	 * Testando metodo top, ao remover um objeto da pilha
	 */
	@Test
	void topTeste3() {
		for (int i = 0; i < 3; i++) {
			pilha.push("Teste " + i);
		}
		
		Assertions.assertEquals("Teste 2", pilha.top());
		pilha.pop();
		Assertions.assertEquals("Teste 1", pilha.top());
	}
	
	/**
	 * Testando metodo top, verificando o elemento adicionando um novo objeto
	 */
	@Test
	void topTeste4() {
		for (int i = 0; i < 3; i++) {
			pilha.push("Teste " + i);
		}
		
		Assertions.assertEquals("Teste 2", pilha.top());
		pilha.push("Teste 10");
		Assertions.assertEquals("Teste 10", pilha.top());
	}
	
	/**
	 * Testando o metodo pop ao remover o unico elemento da pilha
	 */
	@Test
	void topTeste5() {
		pilha.push("Teste 1");
		pilha.pop();
		Assertions.assertThrows(EmptyStackException.class, () -> {
			pilha.top();
		});
		
	}
	
	/**
	 * Testando metodo clear, limpando uma pilha com elementos
	 */
	@Test
	void clearTeste1() {
		for (int i = 0; i < 3; i++) {
			pilha.push("Teste " + i);
		}
		
		Assertions.assertEquals("[Teste 2, Teste 1, Teste 0]", pilha.toString());
		pilha.clear();
		Assertions.assertEquals("[]", pilha.toString());
	}
	
	/**
	 * Testando metodo clear, limpando um pilha vazia
	 */
	@Test
	void clearTeste2() {
		Assertions.assertEquals("[]", pilha.toString());
		pilha.clear();
		Assertions.assertEquals("[]", pilha.toString());
	}
	
	/**
	 * Testando metodo clear, verificando o tamanho da pilha
	 */
	@Test
	void clearTeste3() {
		for (int i = 0; i < 3; i++) {
			pilha.push("Teste " + i);
		}
		
		Assertions.assertEquals(3, pilha.size());
		pilha.clear();
		Assertions.assertEquals(0, pilha.size());
	}
	
	/**
	 * Testando metodo toString, verificando uma pilha com elementos
	 */
	@Test
	void toStringTeste1() {
		for (int i = 0; i < 3; i++) {
			pilha.push("Teste " + i);
		}
		Assertions.assertEquals("[Teste 2, Teste 1, Teste 0]", pilha.toString());
		
	}
	
	/**
	 * Testando metodo toString, verificando uma pilha vazia
	 */
	@Test
	void toStringTeste2() {
		Assertions.assertEquals("[]", pilha.toString());
		
	}
	
	/**
	 * Testando metodo toString, verificando uma pilha com elementos apos da um push
	 */
	@Test
	void toStringTeste3() {
		for (int i = 0; i < 3; i++) {
			pilha.push("Teste " + i);
		}
		Assertions.assertEquals("[Teste 2, Teste 1, Teste 0]", pilha.toString());
		pilha.push("Teste 1");
		Assertions.assertEquals("[Teste 1, Teste 2, Teste 1, Teste 0]", pilha.toString());
		
	}
	
	/**
	 * Testando metodo toString, verificando uma pilha com elementos apos da um pop
	 */
	@Test
	void toStringTeste4() {
		for (int i = 0; i < 3; i++) {
			pilha.push("Teste " + i);
		}
		Assertions.assertEquals("[Teste 2, Teste 1, Teste 0]", pilha.toString());
		pilha.pop();
		Assertions.assertEquals("[Teste 1, Teste 0]", pilha.toString());
		
	}
	

}
