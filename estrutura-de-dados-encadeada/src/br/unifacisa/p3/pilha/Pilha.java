package br.unifacisa.p3.pilha;

import br.unifacisa.p3.abstrata.Colecao;
import br.unifacisa.p3.utils.No;
import br.unifacisa.p3.utils.Validar;

/**
 * Implementacao da classe Pilha (FILO)
 * 
 * @author Pedro
 */
public class Pilha<T> extends Colecao<T> {
	
	/**
	 * Metodo para criar o objeto
	 */
	public Pilha() {
		super();
	}

	/**
	 * Metodo usado para colocar o elemento no top da Pilha
	 * 
	 * @param obj T
	 */
	public void push(T obj) {
		Validar.checkNull(obj);

		No<T> novoObj = new No<T>(obj);
		if (inicio == null) { 
			inicio = novoObj;
			inicio.setAnterior(null);
		} else { 
			No<T> aux = this.inicio;
			this.inicio = novoObj;
			novoObj.setProximo(aux);
			aux.setAnterior(inicio);

		}
		inseridos++;
	}

	/**
	 * Metodo usado para remover e retornar o objeto removido
	 * 
	 * @return T
	 */
	public T pop() {
		Validar.checkPilha(this.inseridos);
		No<T> removeu = this.inicio;
		if (inseridos == 1) {
			this.inicio = null;
			inseridos = 0;
		} else {
			this.inicio = this.inicio.getProximo();
			this.inicio.setAnterior(null);
			inseridos--;
		}
		return removeu.getObj();
	}

	/**
	 * Metodo usado para retornar o topo da Pilha sem remover
	 * 
	 * @return T
	 */
	public T top() {
		Validar.checkPilha(this.inseridos);
		return inicio.getObj();
	}
	
	/**
	 * Metodo usado para limpar a Pilha
	 */
	public void clear() {
		inicio = null;
		inseridos = 0;
	}

}
