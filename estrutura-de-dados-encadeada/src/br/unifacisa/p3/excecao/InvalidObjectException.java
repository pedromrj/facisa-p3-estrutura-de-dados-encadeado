package br.unifacisa.p3.excecao;

public class InvalidObjectException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InvalidObjectException(String msg) {
		super(msg);
	}

}
