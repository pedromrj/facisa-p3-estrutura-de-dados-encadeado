package br.unifacisa.p3.fila;

import br.unifacisa.p3.abstrata.Colecao;
import br.unifacisa.p3.utils.No;
import br.unifacisa.p3.utils.Validar;

/**
 * implementacao da classe Fila (FIFO)
 * 
 * @author Pedro
 */
public class Fila<T> extends Colecao<T> {

	private No<T> fim;

	/**
	 * Metodo usado para enfileira o objeto na Fila, ou seja adicionar o objeto ao
	 * final da Fila
	 * @param obj T
	 */
	public void enqueue(T obj) {
		Validar.checkNull(obj);

		No<T> novoObj = new No<T>(obj);
		if (inicio == null) {
			inicio = novoObj;
			fim = novoObj;
		} else {
			No<T> aux = fim;
			fim = novoObj;
			fim.setAnterior(aux);
			aux.setProximo(fim);
		}
		inseridos++;
	}

	/**
	 * Metodo usado para retornar o primeiro elemento da Fila
	 * @return T
	 */
	public T front() {
		Validar.checkFila(this.inseridos);
		return this.inicio.getObj();
	}

	/**
	 * Metodo usado para retirar o primeiro elemento da Fila e retorna
	 * @return T
	 */
	public T dequeue() {
		Validar.checkFila(this.inseridos);
		No<T> aux = this.inicio;
		if (inseridos == 1) {
			this.inicio = null;
			this.fim = null;
			inseridos = 0;

		} else {
			this.inicio = inicio.getProximo();
			inicio.setAnterior(null);
			inseridos--;
		}

		return aux.getObj();
	}

}
