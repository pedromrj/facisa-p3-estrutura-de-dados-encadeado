package br.unifacisa.p3.fila.teste;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.unifacisa.p3.excecao.EmptyQueueException;
import br.unifacisa.p3.excecao.InvalidObjectException;
import br.unifacisa.p3.fila.Fila;

/**
 * Testes da classe Fila
 * 
 * @author Pedro
 *
 */
class FilaTeste {

	/**
	 * Objeto usado para testa a Classe Fila
	 */
	private Fila<String> fila;

	/**
	 * Metodo usado para resetar o objeto fila
	 */
	@BeforeEach
	void setUp() throws Exception {
		this.fila = new Fila<String>();
	}

	/**
	 * Testando o metodo enqueue para enfileirar os objetos
	 * 
	 * @throws InvalidObjectException
	 */
	@Test
	void enqueueTeste1() {

		for (int i = 0; i < 10; i++) {
			fila.enqueue("Teste " + i);
		}

		Assertions.assertEquals(10, fila.size());

		fila.enqueue("Teste 1");

		Assertions.assertEquals(
				"[Teste 0, Teste 1, Teste 2, Teste 3, Teste 4, Teste 5, Teste 6, Teste 7, Teste 8, Teste 9, Teste 1]",
				fila.toString());

	}

	/**
	 * Testanto o metodo enqueue passando como argumento null
	 */
	@Test
	void enqueueTeste2() {

		Assertions.assertThrows(InvalidObjectException.class, () -> {
			fila.enqueue(null);
		});

	}
	
	/**
	 * Testando metodo enqueue, verificando se passando null a fila continua vazia
	 */
	@Test
	void enqueueTeste3() {
		Assertions.assertThrows(InvalidObjectException.class, () -> {
			fila.enqueue(null);
		});
		
		Assertions.assertEquals("[]", fila.toString());
	}

	/**
	 * Testando o metodo dequeue para retirar o primeiro objeto da fila
	 * 
	 * @throws InvalidObjectException
	 */
	@Test
	void dequeueTeste1() {

		for (int i = 0; i < 3; i++) {
			fila.enqueue("Teste " + i);
		}

		fila.dequeue();

		Assertions.assertEquals("[Teste 1, Teste 2]", fila.toString());

	}
	
	/**
	 * Testando o metodo dequeue, verificando o objeto que � retornado
	 */
	@Test
	void dequeueTeste2() {
		for (int i = 0; i < 3; i++) {
			fila.enqueue("Teste " + i);
		}
		
		Assertions.assertEquals("Teste 0", fila.dequeue());
		Assertions.assertEquals("[Teste 1, Teste 2]", fila.toString());
	}

	/**
	 * Testando o metodo dequeue com uma fila vazia
	 */
	@Test
	void dequeueTeste3() {

		Assertions.assertThrows(EmptyQueueException.class, () -> {
			fila.dequeue();
		});

	}
	
	/**
	 * Testando o metodo dequeue com um elemento na fila
	 */
	@Test
	void dequeueTeste4() {
		fila.enqueue("Teste 1");
		Assertions.assertEquals("Teste 1", fila.dequeue());
		Assertions.assertEquals("[]", fila.toString());
	}

	/**
	 * Testando o metodo front para buscando o primeiro elemento da fila
	 * 
	 */
	@Test
	void frontTeste1() {

		for (int i = 0; i < 10; i++) {
			fila.enqueue("Teste " + i);
		}

		Assertions.assertEquals("Teste 0", fila.front());

	}

	/**
	 * Testando buscar primeiro elemento da fila vazia
	 */
	@Test
	void frontTeste2() {

		Assertions.assertThrows(EmptyQueueException.class, () -> {
			fila.front();
		});

	}
	
	/**
	 * Testando metodo front ao dar denqueue no primeiro elemento da fila
	 */
	@Test
	void frontTeste3() {
		for (int i = 0; i < 10; i++) {
			fila.enqueue("Teste " + i);
		}

		Assertions.assertEquals("Teste 0", fila.front());
		fila.dequeue();
		Assertions.assertEquals("Teste 1", fila.front());
	}
	
	/**
	 * Testando o metodo front ao adicionar mais um elemento a fila
	 */
	@Test
	void frontTeste4() {
		for (int i = 0; i < 10; i++) {
			fila.enqueue("Teste " + i);
		}
		Assertions.assertEquals("Teste 0", fila.front());
		fila.enqueue("Teste 10");
		Assertions.assertEquals("Teste 0", fila.front());
		
	}
	
	/**
	 * Testando o metodo front ao remover o unico elemento da fila
	 */
	@Test
	void frontTeste5() {
		fila.enqueue("Teste 1");
		fila.dequeue();
		Assertions.assertThrows(EmptyQueueException.class, () -> {
			fila.front();
		});
		
	}

	/**
	 * Metodo da classe abstract Colecao, size para testar tamanho
	 */
	@Test
	void sizeTeste1() {

		for (int i = 0; i < 3; i++) {
			fila.enqueue("Teste " + i);
		}

		Assertions.assertEquals(3, fila.size());

	}

	/**
	 * Metodo da classe abstract Colecao, size para testar tamanho com a fila vazia
	 */
	@Test
	void sizeTeste2() {

		Assertions.assertEquals(0, fila.size());

	}
	
	/**
	 * Testando metodo size, ao dar dequeue na fila
	 */
	@Test
	void sizeTeste3() {
		for (int i = 0; i < 3; i++) {
			fila.enqueue("Teste " + i);
		}

		Assertions.assertEquals(3, fila.size());
		fila.dequeue();
		Assertions.assertEquals(2, fila.size());
	}
	
	/**
	 * Testando metodo size, em um fila com elementos adicionar mais elementos
	 */
	@Test
	void sizeTeste4() {
		for (int i = 0; i < 3; i++) {
			fila.enqueue("Teste " + i);
		}
		
		Assertions.assertEquals(3, fila.size());
		fila.enqueue("Teste 1");
		Assertions.assertEquals(4, fila.size());
	}

	/**
	 * Metodo da classe abstract Colecao, isEmpty para verificar se a fila esta
	 * vazia
	 */
	@Test
	void isEmptyTeste1() {

		Assertions.assertTrue(fila.isEmpty());

	}

	/**
	 * Metodo da classe abstract Colecao, isEmpty para verificar se a fila esta com
	 * elemento
	 */
	@Test
	void isEmptyTeste2() {

		for (int i = 0; i < 3; i++) {
			fila.enqueue("Teste " + i);
		}

		Assertions.assertFalse(fila.isEmpty());

	}
	
	/**
	 * Testando metodo isEmpty, logo ao desenfileira a fila
	 */
	@Test
	void isEmptyTeste3() {
		for (int i = 0; i < 3; i++) {
			fila.enqueue("Teste " + i);
		}
		
		Assertions.assertFalse(fila.isEmpty());
		fila.dequeue();
		Assertions.assertFalse(fila.isEmpty());

	}
	
	/**
	 * Testando metodo isEmpty, em uma fila com elementos adicionando mais elementos
	 */
	@Test
	void isEmptyTeste4() {
		for (int i = 0; i < 3; i++) {
			fila.enqueue("Teste " + i);
		}
		Assertions.assertFalse(fila.isEmpty());
		fila.enqueue("Teste 1");
		Assertions.assertFalse(fila.isEmpty());
	}
	
	/**
	 * Testando metodo toString, verificando uma fila com elementos
	 */
	@Test
	void toStringTeste1() {
		for (int i = 0; i < 3; i++) {
			fila.enqueue("Teste " + i);
		}
		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2]", fila.toString());
		
	}
	
	/**
	 * Testando metodo toString, verificando uma fila vazia
	 */
	@Test
	void toStringTeste2() {
		Assertions.assertEquals("[]", fila.toString());
		
	}
	
	/**
	 * Testando metodo toString, verificando uma fila com elementos apos da um enqueue
	 */
	@Test
	void toStringTeste3() {
		for (int i = 0; i < 3; i++) {
			fila.enqueue("Teste " + i);
		}
		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2]", fila.toString());
		fila.enqueue("Teste 1");
		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2, Teste 1]", fila.toString());
		
	}
	
	/**
	 * Testando metodo toString, verificando uma pilha com elementos apos da um dequeue
	 */
	@Test
	void toStringTeste4() {
		for (int i = 0; i < 3; i++) {
			fila.enqueue("Teste " + i);	
		}
		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2]", fila.toString());
		fila.dequeue();
		Assertions.assertEquals("[Teste 1, Teste 2]", fila.toString());
		
	}
}
