package br.unifacisa.p3.abstrata;

import br.unifacisa.p3.utils.No;

/**
 * Implementacao classe Abstrata colecao
 * @author Pedro
 */
public abstract class Colecao<T> {
	
	protected No<T> inicio;

	protected int inseridos;
	
	/**
	 * Metodo para instanciar um colecao vazia
	 */
	public Colecao() {
		super();
		this.inicio = null;
		this.inseridos = 0;
	}

	/**
	 * Metodo usado para retornar o tamanho da Colecao
	 * @return int
	 */
	public int size() {
		return inseridos;
	}
	
	/**
	 * Metodo usado para verificar se a colecao esta vazia
	 * @return boolean
	 */
	public boolean isEmpty() {
		return this.inseridos == 0;
	}
	
	
	/**
	 * Metodo usado para mostra a colecao em formato de texto
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		if (inseridos > 0) {
			No<T> inicio = this.inicio;
			while (inicio.getProximo() != null) {
				builder.append(inicio + ", ");
				inicio = inicio.getProximo();
			}
			builder.append(inicio);
		}
		builder.append("]");
		return builder.toString();
	}
}
