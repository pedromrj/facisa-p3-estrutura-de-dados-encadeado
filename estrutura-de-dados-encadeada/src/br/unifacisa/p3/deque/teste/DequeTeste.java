package br.unifacisa.p3.deque.teste;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.unifacisa.p3.deque.Deque;
import br.unifacisa.p3.excecao.EmptyDequeException;
import br.unifacisa.p3.excecao.InvalidObjectException;

/**
 * Testes da classe Deque
 * 
 * @author Pedro
 *
 */
class DequeTeste {

	/**
	 * Objeto usado para testes;
	 */
	private Deque<String> deque;

	/**
	 * Metodo usado para resetar o deque
	 * 
	 * @throws Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		this.deque = new Deque<String>();
	}

	/**
	 * Testando metodo insertFirst, inserir valores no inicio do Deque
	 * 
	 */
	@Test
	void insertFirstTeste1() {

		for (int i = 0; i < 3; i++) {
			deque.insertFirst("Teste " + i);
		}

		Assertions.assertEquals("[Teste 2, Teste 1, Teste 0]", deque.toString());

		deque.insertFirst("Teste 20");

		Assertions.assertEquals("[Teste 20, Teste 2, Teste 1, Teste 0]", deque.toString());

		Assertions.assertEquals(4, deque.size());

	}

	/**
	 * Testando metodo insertFirst, passado como argumento null
	 */
	@Test
	void insertFirstTeste2() {
		Assertions.assertThrows(InvalidObjectException.class, () -> {
			deque.insertFirst(null);
		});
	}

	/**
	 * Testando metodo insertLast, inserindo obj no final do Deque
	 * 
	 */
	@Test
	void insertLastTeste1() {
		for (int i = 0; i < 3; i++) {
			deque.insertFirst("Teste " + i);
		}

		Assertions.assertEquals("[Teste 2, Teste 1, Teste 0]", deque.toString());

		deque.insertLast("Teste 20");

		Assertions.assertEquals("[Teste 2, Teste 1, Teste 0, Teste 20]", deque.toString());

		Assertions.assertEquals(4, deque.size());
	}

	/**
	 * Testando metodo insertLast, inserido obj no final do Deque
	 */
	@Test
	void insertLastTeste2() {
		Assertions.assertThrows(InvalidObjectException.class, () -> {
			deque.insertLast(null);
		});
	}

	/**
	 * Testando metodo insertLast, inserindo em um deque vazio
	 */
	@Test
	void insertLastTeste3() {
		for (int i = 0; i < 3; i++) {
			deque.insertLast("Teste " + i);
		}

		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2]", deque.toString());
	}

	/**
	 * Testando metodo removeFirst, remover o primeiro elemento do Deque
	 * 
	 */
	@Test
	void removeFirstTeste1() {
		for (int i = 0; i < 3; i++) {
			deque.insertFirst("Teste " + i);
		}

		Assertions.assertEquals("[Teste 2, Teste 1, Teste 0]", deque.toString());

		Assertions.assertEquals(3, deque.size());

		deque.removeFirst();

		Assertions.assertEquals("[Teste 1, Teste 0]", deque.toString());

		Assertions.assertEquals(2, deque.size());
	}

	/**
	 * Testando metodo removeFirst, removendo o primeiro elemento do Deque vazio
	 */
	@Test
	void removeFirstTeste2() {
		Assertions.assertThrows(EmptyDequeException.class, () -> {
			deque.removeFirst();
		});
	}

	/**
	 * Testando metodo removeFirst, removendo o elemento com um elemento no deque
	 */
	@Test
	void removeFirstTeste3() {
		deque.insertFirst("Teste 1");

		Assertions.assertEquals("[Teste 1]", deque.toString());
		deque.removeFirst();
		Assertions.assertEquals("[]", deque.toString());

	}

	/**
	 * Testando metodo removeFirst, removendo o todos os elementos do deque
	 */
	@Test
	void removeFirstTeste4() {
		for (int i = 0; i < 3; i++) {
			deque.insertFirst("Teste " + i);
		}
		Assertions.assertEquals("[Teste 2, Teste 1, Teste 0]", deque.toString());
		deque.removeFirst();
		Assertions.assertEquals("[Teste 1, Teste 0]", deque.toString());
		deque.removeFirst();
		Assertions.assertEquals("[Teste 0]", deque.toString());
		deque.removeFirst();
		Assertions.assertEquals("[]", deque.toString());
	}

	/**
	 * Testando metodo removeFirst, removendo os primeiros elementos adicionando no
	 * final
	 */
	@Test
	void removeFirstTeste5() {
		for (int i = 0; i < 3; i++) {
			deque.insertLast("Teste " + i);
		}
		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2]", deque.toString());
		deque.removeFirst();
		Assertions.assertEquals("[Teste 1, Teste 2]", deque.toString());
		deque.removeFirst();
		Assertions.assertEquals("[Teste 2]", deque.toString());
		deque.removeFirst();
		Assertions.assertEquals("[]", deque.toString());
	}

	/**
	 * Testando metodo removeLast, removendo elemento de uma Deque com elementos
	 * 
	 */
	@Test
	void removeLastTeste1() {
		for (int i = 0; i < 3; i++) {
			deque.insertLast("Teste " + i);
		}

		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2]", deque.toString());

		Assertions.assertEquals(3, deque.size());

		deque.removeLast();

		Assertions.assertEquals("[Teste 0, Teste 1]", deque.toString());

		Assertions.assertEquals(2, deque.size());
	}

	/**
	 * Testando metodo removeLast, removendo elemento de um deque vazio
	 */
	@Test
	void removeLastTeste2() {
		Assertions.assertThrows(EmptyDequeException.class, () -> {
			deque.removeLast();
		});
	}

	/**
	 * Testando metodo removeLast, removendo todos os valores do deque
	 */
	@Test
	void removeLastTeste3() {
		for (int i = 0; i < 3; i++) {
			deque.insertLast("Teste " + i);
		}

		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2]", deque.toString());
		deque.removeLast();
		Assertions.assertEquals("[Teste 0, Teste 1]", deque.toString());
		deque.removeLast();
		Assertions.assertEquals("[Teste 0]", deque.toString());
		deque.removeLast();
		Assertions.assertEquals("[]", deque.toString());

	}

	/**
	 * Testando metodo removeLast, removendo elemento depois de adicionar no inicio
	 */
	@Test
	void removeLastTeste4() {
		for (int i = 0; i < 3; i++) {
			deque.insertFirst("Teste " + i);
		}
		Assertions.assertEquals("[Teste 2, Teste 1, Teste 0]", deque.toString());
		deque.removeLast();
		Assertions.assertEquals("[Teste 2, Teste 1]", deque.toString());
		deque.removeLast();
		Assertions.assertEquals("[Teste 2]", deque.toString());
		deque.removeLast();
		Assertions.assertEquals("[]", deque.toString());

	}
	
	/**
	 * Testando metodo removeLast, removendo um unico elemento no deque
	 */
	@Test
	void removeLastTeste5() {
		deque.insertFirst("Teste 1");
		Assertions.assertEquals("[Teste 1]", deque.toString());
		deque.removeLast();
		Assertions.assertEquals("[]", deque.toString());
	}

	/**
	 * Testando metodo removeByIndex, passando como argumento valores negativo
	 */
	@Test
	void removeByIndexTeste1() {
		Assertions.assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
			deque.removeByIndex(-1);
		});
	}

	/**
	 * Testando metodo removeByIndex, passando como argumento o index valido
	 */
	@Test
	void removeByIndexTeste2() {
		for (int i = 0; i < 3; i++) {
			deque.insertFirst("Teste " + i);
		}

		deque.removeByIndex(1);

		Assertions.assertEquals("[Teste 2, Teste 0]", deque.toString());

		deque.removeByIndex(1);

		Assertions.assertEquals("[Teste 2]", deque.toString());

		deque.removeByIndex(0);

		Assertions.assertEquals("[]", deque.toString());

	}

	/**
	 * Testando metodo removeByIndex, testando remover o unico elemento adicinado no comeco
	 * 
	 */
	@Test
	void removeByIndexTeste3() {
		deque.insertFirst("Teste 1");
		Assertions.assertEquals("[Teste 1]", deque.toString());
		deque.removeByIndex(0);
		Assertions.assertEquals("[]", deque.toString());

	}

	/**
	 * Testando metodo removeByIndex, testando remover o ultimo elemento da lista
	 */
	@Test
	void removeByIndexTeste4() {
		deque.insertFirst("Teste 1");
		deque.insertFirst("Teste 2");
		deque.insertFirst("Teste 3");
		Assertions.assertEquals("[Teste 3, Teste 2, Teste 1]", deque.toString());
		deque.removeByIndex(2);
		Assertions.assertEquals("[Teste 3, Teste 2]", deque.toString());
		deque.removeByIndex(1);
		Assertions.assertEquals("[Teste 3]", deque.toString());
		deque.removeByIndex(0);
		Assertions.assertEquals("[]", deque.toString());

	}

	/**
	 * Testando metodo removeByIndex, testando remover o unico elemento adicionado no final
	 * 
	 */
	@Test
	void removeByIndexTeste5() {
		deque.insertLast("Teste 1");
		Assertions.assertEquals("[Teste 1]", deque.toString());
		deque.removeByIndex(0);
		Assertions.assertEquals("[]", deque.toString());
	}

	/**
	 * Testando metodo removeByValue, passando como argumento objeto que esta no
	 * Deque
	 */
	@Test
	void removeByValueTeste1() {
		for (int i = 0; i < 3; i++) {
			deque.insertFirst("Teste " + i);
		}

		deque.removeByValue("Teste 1");

		Assertions.assertEquals("[Teste 2, Teste 0]", deque.toString());

		deque.removeByValue("Teste 0");

		Assertions.assertEquals("[Teste 2]", deque.toString());

		deque.removeByValue("Teste 2");

		Assertions.assertEquals("[]", deque.toString());
	}

	/**
	 * Testando metodo removeByValue, removendo o unico elemento do deque
	 */
	@Test
	void removeByValueTeste2() {

		deque.insertFirst("Teste 1");

		deque.removeByValue("Teste 1");

		Assertions.assertEquals("[]", deque.toString());

	}

	/**
	 * Testando metodo removeByValue, removendo os ultimos elementos do deque
	 * 
	 */
	@Test
	void removeByValueTeste3() {

		deque.insertFirst("Teste 1");
		deque.insertFirst("Teste 2");
		deque.insertFirst("Teste 3");

		deque.removeByValue("Teste 1");

		Assertions.assertEquals("[Teste 3, Teste 2]", deque.toString());
		deque.removeByValue("Teste 2");
		Assertions.assertEquals("[Teste 3]", deque.toString());
		deque.removeByValue("Teste 3");
		Assertions.assertEquals("[]", deque.toString());

	}

	/**
	 * Testando o metodo removeByValue, passando como argumento null
	 */
	@Test
	void removeByValueTeste4() {
		Assertions.assertThrows(InvalidObjectException.class, () -> {
			deque.removeByValue(null);
		});
	}

	/**
	 * Testando metodo removeByValue, passando um elemento que n�o existe
	 */
	@Test
	void removeByValueTeste5() {
		deque.insertFirst("Teste 1");

		Assertions.assertFalse(deque.removeByValue("Teste 2"));
		Assertions.assertEquals("[Teste 1]", deque.toString());
		Assertions.assertTrue(deque.removeByValue("Teste 1"));
		Assertions.assertEquals("[]", deque.toString());

	}

	/**
	 * Testando metodo removeByValue, removendo o unico elemento adicionado no final
	 */
	@Test
	void removeByValueTeste() {
		deque.insertLast("Teste 1");
		Assertions.assertEquals("[Teste 1]", deque.toString());
		deque.removeByValue("Teste 1");
		Assertions.assertEquals("[]", deque.toString());

	}

	/**
	 * Testando metodo size da classe abstract Colecao, adicionando alguns elemento
	 * e comparando seu tamanho;
	 */
	@Test
	void sizeTeste1() {

		for (int i = 0; i < 3; i++) {
			deque.insertFirst("Teste " + i);
		}

		Assertions.assertEquals(3, deque.size());

		deque.removeFirst();
		Assertions.assertEquals(2, deque.size());
		deque.removeLast();
		Assertions.assertEquals(1, deque.size());

		for (int i = 0; i < 3; i++) {
			deque.insertFirst("Teste " + i);
		}

		Assertions.assertEquals(4, deque.size());

		deque.removeByIndex(3);
		Assertions.assertEquals(3, deque.size());

		deque.removeByValue("Teste 0");
		Assertions.assertEquals(2, deque.size());

	}

	/**
	 * Testando metodo size da classe abstrata Colecao, verificando o tamanho vazio
	 * do Deque;
	 */
	@Test
	void sizeTeste2() {

		Assertions.assertEquals(0, deque.size());

	}

	/**
	 * Testando metodo isEmpty da classe abstrata Colecao, verificando se o Deque
	 * esta vazio;
	 */
	@Test
	void isEmptyTeste1() {

		Assertions.assertTrue(deque.isEmpty());

	}

	/**
	 * Testando metodo size da classe abstrata Colecao, verificando se o Deque
	 * possui elementos;
	 * 
	 */
	@Test
	void isEmptyTeste2() {

		for (int i = 0; i < 3; i++) {
			deque.insertFirst("Teste " + i);
		}

		Assertions.assertFalse(deque.isEmpty());

	}
	
	/**
	 * Testando metodo toString, verificando o objeto em formato de texto
	 */
	@Test
	void toStringTeste1() {
		for (int i = 0; i < 3; i++) {
			deque.insertFirst("Teste " + i);
		}
		
		Assertions.assertEquals("[Teste 2, Teste 1, Teste 0]", deque.toString());
	}

}
