package br.unifacisa.p3.deque;

import br.unifacisa.p3.abstrata.Colecao;
import br.unifacisa.p3.utils.No;
import br.unifacisa.p3.utils.Validar;

/**
 * Implementacao da classe Deque
 * 
 * @author Pedro
 */
public class Deque<T> extends Colecao<T> {

	No<T> fim;

	/**
	 * Metodo usado para inserir objeto no comeco do Deque
	 * 
	 * @param obj T
	 */
	public void insertFirst(T obj) {
		Validar.checkNull(obj);

		No<T> novoObj = new No<T>(obj);
		if (inicio == null) {
			inicio = novoObj;
			fim = novoObj;
		} else {
			No<T> aux = inicio;
			aux.setAnterior(novoObj);
			inicio = novoObj;
			novoObj.setProximo(aux);
		}
		inseridos++;
	}

	/**
	 * Metodo usado para inserir objeto no final do Deque
	 * 
	 * @param obj T
	 */
	public void insertLast(T obj) {
		Validar.checkNull(obj);

		No<T> novoObj = new No<T>(obj);
		if (inicio == null) {
			inicio = novoObj;
			fim = novoObj;
		} else {
			No<T> aux = fim;
			novoObj.setAnterior(aux);
			fim = novoObj;
			aux.setProximo(fim);
		}
		inseridos++;
	}

	/**
	 * Metodo usado para remove o primeiro objeto do Deque
	 */
	public void removeFirst() {
		Validar.checkDeque(this.inseridos);
		if (inseridos == 1) {
			inicio = null;
			fim = null;
		} else {
			inicio.getProximo().setAnterior(null);
			inicio = inicio.getProximo();

		}
		inseridos--;
	}
	
	
	/**
	 * Metodo usado para remover o elemento da lista pelo objeto passado como
	 * argumento. Necessario a implementação do metodo Equals
	 * 
	 * @param obj T
	 * @return boolean
	 */
	public boolean removeByValue(T obj) {
		Validar.checkNull(obj);
		boolean removeu = false;
		No<T> inicio = this.inicio;
		for (int i = 0; i < inseridos; i++) {
			if (inicio.getObj().equals(obj) && i == 0) {
				removeFirst();
				removeu = true;
				break;
			} else if (inicio.getProximo() != null) {
				if (inicio.getProximo().getObj().equals(obj)) {
					inicio.setProximo(inicio.getProximo().getProximo());
					if (i == inseridos - 1 && inseridos != 0) {
						inicio.getProximo().setAnterior(inicio);
					}
					inseridos--;
					removeu = true;
					break;
				}

			}
			inicio = inicio.getProximo();
		}
		return removeu;
	}


	/**
	 * Metodo usado para remove o objeto por index
	 * 
	 * @param indice int
	 */
	public void removeByIndex(int indice) {
		Validar.checkArray(indice, this.inseridos);
		No<T> inicio = this.inicio;
		if (indice == 0) { 
			removeFirst();
		} else if (indice == inseridos - 1 && inseridos - 1 != 0) { 
			removeLast();
		} else {
			for (int i = 0; i < indice - 1; i++) {
				inicio = inicio.getProximo();
			}
			inicio.setProximo(inicio.getProximo().getProximo());
			inicio.getProximo().setAnterior(inicio);
			inseridos--;
		}

	}
	
	

	/**
	 * Metodo usado para remove o ultimo elemento do Deque
	 */
	public void removeLast() {
		Validar.checkDeque(this.inseridos);
		if (fim.getAnterior() != null) {
			No<T> aux = fim.getAnterior();
			aux.setProximo(null);
			fim = aux;
		} else {
			fim = null;
			inicio = null;
		}
		inseridos--;
	}
}
