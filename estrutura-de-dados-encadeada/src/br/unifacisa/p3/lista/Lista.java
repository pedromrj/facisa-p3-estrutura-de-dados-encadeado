package br.unifacisa.p3.lista;

import br.unifacisa.p3.abstrata.Colecao;
import br.unifacisa.p3.utils.No;
import br.unifacisa.p3.utils.Validar;

/**
 * implementacao da lista encadeada
 * 
 * @author Pedro
 */
public class Lista<T> extends Colecao<T> {

	private No<T> fim;

	/**
	 * Metodo para criar uma lista vazia
	 */
	public Lista() {
		super();
		fim = null;
	}

	/**
	 * Metodo usado para adicionar o elemento a lista encadeada
	 * 
	 * @param obj T
	 */
	public void add(T obj) {
		Validar.checkNull(obj);

		No<T> novoObj = new No<T>(obj);
		if (inicio == null) {
			inicio = novoObj;
			fim = novoObj;
			novoObj.setAnterior(null);
		} else {
			No<T> aux = fim;
			novoObj.setAnterior(aux);
			fim = novoObj;
			aux.setProximo(fim);
		}
		inseridos++;
	}

	/**
	 * Metodo para consultar o elemento que esta no indice passado como argumento
	 * 
	 * @param indice int
	 * @return T
	 */
	public T get(int indice) {
		Validar.checkArray(indice, this.inseridos);

		No<T> inicio = this.inicio;
		T achou = null;
		for (int i = 0; i < indice; i++) {
			inicio = inicio.getProximo();
		}
		achou = inicio.getObj();
		return achou;
	}

	/**
	 * Metodo usado para retornar a primeira ocorrencia do obj na lista encadeada
	 * 
	 * @param obj T
	 * @return int
	 */
	public int indexOf(T obj) {
		Validar.checkNull(obj);

		No<T> inicio = this.inicio;
		int achou = -1;
		for (int i = 0; i < inseridos; i++) {
			if (inicio.getObj().equals(obj)) {
				achou = i;
				break;
			}
			inicio = inicio.getProximo();
		}
		return achou;
	}

	/**
	 * Metodo usado para remover o objeto pelo indice passado como argumento
	 * 
	 * @param indice int
	 */
	public void remove(int indice) {
		Validar.checkArray(indice, this.inseridos);
		No<T> inicio = this.inicio;

		if (indice == 0) {
			removeFirst();
		} else if (indice == inseridos - 1 && inseridos - 1 != 0) {
			this.fim = fim.getAnterior();
			fim.setProximo(null);
		} else {
			for (int i = 0; i < indice - 1; i++) {
				inicio = inicio.getProximo();
			}
			inicio.setProximo(inicio.getProximo().getProximo());
			inicio.getProximo().setAnterior(inicio);
		}
		inseridos--;
	}

	/**
	 * Metodo usado para remover o elemento da lista pelo objeto passado como
	 * argumento. Necessario a implementação do metodo Equals
	 * 
	 * @param obj T
	 * @return boolean
	 */
	public boolean remove(T obj) {
		Validar.checkNull(obj);
		boolean removeu = false;
		No<T> inicio = this.inicio;
		for (int i = 0; i < inseridos; i++) {
			if (inicio.getObj().equals(obj) && i == 0) {
				removeFirst();
				inseridos--;
				removeu = true;
				break;
			} else if (inicio.getProximo() != null) {
				if (inicio.getProximo().getObj().equals(obj)) {
					inicio.setProximo(inicio.getProximo().getProximo());
					if (i == inseridos - 1 && inseridos != 0) {
						inicio.getProximo().setAnterior(inicio);
					}
					removeu = true;
					inseridos--;
					break;
				}

			}
			inicio = inicio.getProximo();
		}
		return removeu;
	}

	/**
	 * Metodo usado para auxiliar a remocao do primeiro elemento do metodo remove
	 * por objeto ou indice
	 */
	private void removeFirst() {
		if (inseridos == 1) {
			this.inicio = null;
			this.fim = null;
		} else {
			this.inicio = inicio.getProximo();
			this.inicio.setAnterior(null);
		}
	}

	/**
	 * Metodo usado para concatenar uma lista encadeada com outra
	 * 
	 * @param elementos T
	 */
	public void concat(Lista<T> elementos) {
		Validar.checkNull(elementos);
		if (elementos.size() != 0) {
			fim.setProximo(elementos.inicio);
			fim.getProximo().setAnterior(fim);
			fim = elementos.fim;
			inseridos += elementos.inseridos;
		}
	}

	/**
	 * Metodo usado para substituir o objeto da posicao passada como argumento
	 * 
	 * @param indice int
	 * @param obj    T
	 */
	public void set(int indice, T obj) {
		Validar.checkNull(obj);
		Validar.checkArray(indice, this.inseridos);

		No<T> inicio = this.inicio;
		for (int i = 0; i <= indice - 1; i++) {
			inicio = inicio.getProximo();
		}
		inicio.setObj(obj);
	}
}
