package br.unifacisa.p3.lista.teste;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.unifacisa.p3.excecao.InvalidObjectException;
import br.unifacisa.p3.lista.Lista;

/**
 * Testando a classe Lista
 * 
 * @author Pedro
 *
 */
class ListaTeste {

	/**
	 * Objeto usado para testa a Lista
	 */
	private Lista<String> lista;

	/**
	 * Metodo usado para reseta a lista apos cada teste
	 * 
	 */
	@BeforeEach
	void setUp() throws Exception {
		this.lista = new Lista<String>();
	}

	/**
	 * Testando metodo add, inserindo alguns elemento na lista e verificando o seu
	 * tamanho e seus elementos
	 * 
	 */
	@Test
	void addTeste1() {
		for (int i = 0; i < 4; i++) {
			lista.add("Teste " + i);
		}

		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2, Teste 3]", lista.toString());

		Assertions.assertEquals(4, lista.size());

	}

	/**
	 * Testando metodo add, passando como argumento null
	 */
	@Test
	void addTeste2() {

		Assertions.assertThrows(InvalidObjectException.class, () -> {
			lista.add(null);
		});
	}


	/**
	 * Testando metodo get, obtendo um elemento da lista por um indice
	 * 
	 * @throws InvalidObjectException
	 */
	@Test
	void getTeste1() {
		for (int i = 0; i < 4; i++) {
			lista.add("Teste " + i);
		}

		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2, Teste 3]", lista.toString());

		Assertions.assertEquals("Teste 1", lista.get(1));

	}

	/**
	 * Testando metodo get, passando como argumento um indice negativo
	 * 
	 * @throws InvalidObjectException
	 */
	@Test
	void getTeste2() {
		Assertions.assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
			lista.get(-1);
		});
	}

	/**
	 * Testando metodo get, buscando o index 0 de um lista vazia
	 */
	@Test
	void getTeste3() {
		Assertions.assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
			lista.get(0);
		});
	}

	/**
	 * Testando metodo indexOf, buscando a ocorrencia de diversos objeto dentro da
	 * lista
	 * 
	 * @throws InvalidObjectException
	 */
	@Test
	void indexOfTeste1() {

		for (int i = 0; i < 4; i++) {
			lista.add("Teste " + i);
		}

		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2, Teste 3]", lista.toString());

		Assertions.assertEquals(1, lista.indexOf("Teste 1"));
		Assertions.assertEquals(2, lista.indexOf("Teste 2"));
		Assertions.assertEquals(3, lista.indexOf("Teste 3"));
		Assertions.assertEquals(0, lista.indexOf("Teste 0"));
		

	}

	/**
	 * Testando metodo indexOf, passando como argumento null
	 */
	@Test
	void indexOfTeste2() {
		Assertions.assertThrows(InvalidObjectException.class, () -> {
			lista.indexOf(null);
		});
	}
	
	/**
	 * Testando metodo indexOf, passando como argumento um objeto que n�o existe
	 */
	@Test
	void indexOfTeste3() {
		for (int i = 0; i < 4; i++) {
			lista.add("Teste " + i);
		}
		
		Assertions.assertEquals(-1, lista.indexOf("Teste 10"));
	}

	/**
	 * Testando metodo remove, passando um indice para remove
	 * 
	 */
	@Test
	void removeTeste1() {
		for (int i = 0; i < 4; i++) {
			lista.add("Teste " + i);
		}

		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2, Teste 3]", lista.toString());

		lista.remove(1);
		Assertions.assertEquals("[Teste 0, Teste 2, Teste 3]", lista.toString());

		lista.remove(1);
		Assertions.assertEquals("[Teste 0, Teste 3]", lista.toString());

		lista.remove(1);
		Assertions.assertEquals("[Teste 0]", lista.toString());

		lista.remove(0);
		Assertions.assertEquals("[]", lista.toString());

	}

	/**
	 * Testando metodo remove, passando um indice negativo para remove
	 * 
	 */
	@Test
	void removeTeste2() {
		Assertions.assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
			lista.remove(-1);
		});
	}

	/**
	 * Testando metodo remove, passando argumento null
	 * 
	 */
	@Test
	void removeTeste3() {
		Assertions.assertThrows(InvalidObjectException.class, () -> {
			lista.remove(null);
		});
	}

	/**
	 * Testando metodo remove, passando como argumentos alguns objetos pertencentes
	 * a lista
	 * 
	 * @throws InvalidObjectException
	 */
	@Test
	void removeTeste4() {
		for (int i = 0; i < 4; i++) {
			lista.add("Teste " + i);
		}

		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2, Teste 3]", lista.toString());

		lista.remove("Teste 0");
		Assertions.assertEquals("[Teste 1, Teste 2, Teste 3]", lista.toString());
		lista.remove("Teste 2");
		Assertions.assertEquals("[Teste 1, Teste 3]", lista.toString());
		lista.remove("Teste 3");
		Assertions.assertEquals("[Teste 1]", lista.toString());
		lista.remove("Teste 1");
		Assertions.assertEquals("[]", lista.toString());
	}
	

	/**
	 * Testando metodo remove, removendo o unico elemento da lista;
	 */
	@Test
	void removeTeste5() {

		lista.add("Teste 1");

		lista.remove("Teste 1");

		Assertions.assertEquals("[]", lista.toString());

	}

	/**
	 * Testando metodo remove, removendo o ultimo elemento de uma lista com
	 * elementos
	 */
	@Test
	void removeTeste6() {

		lista.add("Teste 1");
		lista.add("Teste 2");
		lista.add("Teste 3");

		lista.remove("Teste 1");

		Assertions.assertEquals("[Teste 2, Teste 3]", lista.toString());
		lista.remove("Teste 2");
		Assertions.assertEquals("[Teste 3]", lista.toString());
		lista.remove("Teste 3");
		Assertions.assertEquals("[]", lista.toString());

	}
	
	/**
	 * Testando metodo removeByValue, passando um elemento que n�o existe
	 */
	@Test
	void removeTeste7() {
		lista.add("Teste 1");
		
		Assertions.assertFalse(lista.remove("Teste 2"));
		Assertions.assertEquals("[Teste 1]", lista.toString());
		Assertions.assertTrue(lista.remove("Teste 1"));
		Assertions.assertEquals("[]", lista.toString());
		
	}
	
	/**
	 * Testando metodo remove, removendo todos os indices finais
	 */
	@Test
	void removeTeste8() {
		for (int i = 0; i < 4; i++) {
			lista.add("Teste " + i);
		}
		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2, Teste 3]", lista.toString());
		
		lista.remove(3);
		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2]", lista.toString());
		lista.remove(2);
		Assertions.assertEquals("[Teste 0, Teste 1]", lista.toString());
		lista.remove(1);
		Assertions.assertEquals("[Teste 0]", lista.toString());
		lista.remove(0);
		Assertions.assertEquals("[]", lista.toString());
		
	}
	
	/**
	 * Testando metodo remove, removendo todos os indices iniciais
	 */
	@Test
	void removeTeste9() {
		for (int i = 0; i < 4; i++) {
			lista.add("Teste " + i);
		}
		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2, Teste 3]", lista.toString());
		
		lista.remove(0);
		Assertions.assertEquals("[Teste 1, Teste 2, Teste 3]", lista.toString());
		lista.remove(0);
		Assertions.assertEquals("[Teste 2, Teste 3]", lista.toString());
		lista.remove(0);
		Assertions.assertEquals("[Teste 3]", lista.toString());
		lista.remove(0);
		Assertions.assertEquals("[]", lista.toString());
		
	}
	
	/**
	 * Testando metodo remove, removendo o unico elemento pelo indice
	 */
	@Test
	void removeTeste10() {
		lista.add("Teste 1");
		Assertions.assertEquals("[Teste 1]", lista.toString());
		lista.remove(0);
		Assertions.assertEquals("[]", lista.toString());
	}

	/**
	 * Testando metodo concat, passando uma nova Lista como argumento para
	 * concatenar a Lista
	 * 
	 * @throws InvalidObjectException
	 */
	@Test
	void concatTeste1() {
		for (int i = 0; i < 4; i++) {
			lista.add("Teste " + i);
		}

		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2, Teste 3]", lista.toString());

		Lista<String> aux = new Lista<String>();
		aux.add("Teste 5");
		aux.add("Teste 4");

		lista.concat(aux);

		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2, Teste 3, Teste 5, Teste 4]", lista.toString());
	}

	/**
	 * Testando metodo concat, passando como argumento null
	 */
	@Test
	void concatTeste2() {
		Assertions.assertThrows(InvalidObjectException.class, () -> {
			lista.concat(null);
		});
	}
	
	/**
	 * Testando metodo concat, passando uma lista vazia como argumento
	 */
	@Test
	void concatTeste3() {
		for (int i = 0; i < 4; i++) {
			lista.add("Teste " + i);
		}
		Lista<String> aux = new Lista<String>();
		
		lista.concat(aux);
		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2, Teste 3]", lista.toString());
		
	}
	
	/**
	 * Testando metodo concat, verificando o novo fim da lista
	 */
	@Test
	void concatTeste4() {
		for (int i = 0; i < 4; i++) {
			lista.add("Teste " + i);
		}
		
		Lista<String> aux = new Lista<String>();
		aux.add("Teste 5");
		aux.add("Teste 4");
		Assertions.assertEquals("Teste 3", lista.get(3));
		
		lista.concat(aux);
		
		Assertions.assertEquals("Teste 4", lista.get(5));
		
		
	}
	
	/**
	 * Testando metodo concat, verificando o novo tamanho da lista
	 */
	@Test 
	void concatTeste5() {
		for (int i = 0; i < 4; i++) {
			lista.add("Teste " + i);
		}
		
		Lista<String> aux = new Lista<String>();
		aux.add("Teste 5");
		aux.add("Teste 4");
		lista.concat(aux);
		
		Assertions.assertEquals(6, lista.size());
	}
	
	
	/**
	 * Testando metodo set, substituindo elementos da lista
	 */
	@Test
	void setTeste1() {
		for (int i = 0; i < 4; i++) {
			lista.add("Teste " + i);
		}
		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2, Teste 3]", lista.toString());
		lista.set(2, "Teste 9");
		Assertions.assertEquals("[Teste 0, Teste 1, Teste 9, Teste 3]", lista.toString());
		lista.set(3, "Teste 9");
		Assertions.assertEquals("[Teste 0, Teste 1, Teste 9, Teste 9]", lista.toString());
		lista.set(1, "Teste 9");
		Assertions.assertEquals("[Teste 0, Teste 9, Teste 9, Teste 9]", lista.toString());
		lista.set(0, "Teste 9");
		Assertions.assertEquals("[Teste 9, Teste 9, Teste 9, Teste 9]", lista.toString());
	}
	
	/**
	 * Testando metodo set, passando um indice negativo
	 */
	@Test
	void setTeste2() {
		for (int i = 0; i < 4; i++) {
			lista.add("Teste " + i);
		}
		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2, Teste 3]", lista.toString());
		
		Assertions.assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
			lista.set(-1, "Teste 1");
		});
	}
	
	/**
	 * Testando metodo set, passando um indice maior que o tamanho
	 */
	@Test
	void setTeste3() {
		for (int i = 0; i < 4; i++) {
			lista.add("Teste " + i);
		}
		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2, Teste 3]", lista.toString());
		
		Assertions.assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
			lista.set(6, "Teste 1");
		});
	}
	
	/**
	 * Testando metodo set, passando como objeto null
	 */
	@Test
	void setTeste4() {
		for (int i = 0; i < 4; i++) {
			lista.add("Teste " + i);
		}
		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2, Teste 3]", lista.toString());
		
		Assertions.assertThrows(InvalidObjectException.class, () -> {
			lista.set(3, null);
		});
	}
	
	/**
	 * Testando metodo size, metodo da classe abstrata para verificiar o tamanho da lista
	 */
	@Test
	void sizeTeste1() {
		for (int i = 0; i < 4; i++) {
			lista.add("Teste " + i);
		}
		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2, Teste 3]", lista.toString());

		Assertions.assertEquals(4, lista.size());
	}
	
	/**
	 * Testando metodo size, metodo da classe abstrata para verificiar o tamanho da lista vazia
	 */
	@Test
	void sizeTeste2() {
		Assertions.assertEquals(0, lista.size());
	}
	
	/**
	 * Testando metodo size, metodo da classe abstrata para verificiar se a lista ta vazia
	 */
	@Test
	void isEmptyTeste1() {
		for (int i = 0; i < 4; i++) {
			lista.add("Teste " + i);
		}
		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2, Teste 3]", lista.toString());

		Assertions.assertFalse(lista.isEmpty());
	}
	
	/**
	 * Testando metodo size, metodo da classe abstrata para verificiar se a lista ta vazia
	 */
	@Test
	void isEmptyTeste2() {
		Assertions.assertTrue(lista.isEmpty());
	}
	
	/**
	 * Testando metodo toString, metodo da classe abstrata para ver a lista em formato de texto
	 */
	@Test
	void toStringTeste1() {
		for (int i = 0; i < 4; i++) {
			lista.add("Teste " + i);
		}
		
		Assertions.assertEquals("[Teste 0, Teste 1, Teste 2, Teste 3]", lista.toString());

	}
	
}
